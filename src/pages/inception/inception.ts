import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the InceptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inception',
  templateUrl: 'inception.html',
})
export class InceptionPage {

  contador: any = 0;
  inception = InceptionPage;
  params: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.params = JSON.stringify(this.navParams.data);
    this.contador = this.navParams.get("n") || 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InceptionPage');
  }

  goHome(){
    this.navCtrl.setRoot(HomePage);
  }

}
