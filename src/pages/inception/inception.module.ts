import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InceptionPage } from './inception';

@NgModule({
  declarations: [
    InceptionPage,
  ],
  imports: [
    IonicPageModule.forChild(InceptionPage),
  ],
})
export class InceptionPageModule {}
