import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PlayerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-player',
  templateUrl: 'player.html',
})
export class PlayerPage {
  cover: string;
  song: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.cover = this.navParams.get("cover");
    this.song = this.navParams.get("song");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayerPage');
  }

}
