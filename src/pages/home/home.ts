import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavigationPage } from '../navigation/navigation';
import { UiPage } from '../ui/ui';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  navigation = NavigationPage;
  ui = UiPage;

  constructor(public navCtrl: NavController) {

  }

  alertar(){
    alert("pille que si sabe");
  }

}
